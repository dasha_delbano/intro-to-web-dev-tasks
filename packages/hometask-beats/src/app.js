import Swiper, { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/navigation";

Swiper.use([Navigation]);

const swiper = new Swiper(".swiper", {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  centeredSlides: true,
  grabCursor: true,
  loop: true,
  spaceBetween: -55,
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
  },
});
