const body = document.querySelector("body");
const container = document.createElement("div");
container.classList.add("container", "landing");

const header = document.createElement("header");
const main = document.createElement("main");
const footer = document.createElement("footer");

const headerList = ["Discover", "Join", "Sign in"];

header.innerHTML = `
<div class="header__logo">
  <a href="#">
    <img class="header__logo__img" src="./assets/icons/logo.svg" alt="logo" />
    <div class="header__logo__text">Simo</div>
  </a>
</div>
<nav>
</nav>
`;

footer.innerHTML = `
<nav>
  <ul class="footer__nav-menu">
    <li><a href="#">About As</a></li>
    <li><a href="#">Contact</a></li>
    <li><a href="#">CR Info</a></li>
    <li><a href="#">Twitter</a></li>
    <li><a href="#">Facebook</a></li>
  </ul>
</nav>
`;

body.appendChild(container);
container.appendChild(header);
container.appendChild(main);
container.appendChild(footer);
const headerNav = document.querySelector("nav");

function renderLandingPage() {
  container.classList.remove("signUp");
  container.classList.add("landing");
  headerNav.innerHTML = `
  <ul class="header__nav-menu">
  <li><a class= "discover__link-landing" href="#">${headerList[0]}</a></li>
  <li><a class= "join__link" href="#">${headerList[1]}</a></li>
  <li><a class= "sign__link" href="#">${headerList[2]}</a></li>
</ul>
`;
  main.innerHTML = `
  <section class="section-landing">
  <h1 class="title">Feel the music</h1>

  <div class="landing__text">Stream over 10 million songs with one click</div>

  <button class="btn join__link">Join now</button>
</section>
`;
  const joinLink = document.querySelectorAll(".join__link");

  Array.prototype.forEach.call(joinLink, link =>
    link.addEventListener("click", renderSignUpPage)
  );

  const discoverLinkFromLanding = document.querySelector(
    ".discover__link-landing"
  );
  discoverLinkFromLanding.addEventListener("click", renderFeaturePage);
}

renderLandingPage();

function renderSignUpPage() {
  headerNav.innerHTML = `
  <ul class="header__nav-menu">
  <li><a class= "discover__link-signUp" href="#">${headerList[0]}</a></li>
  <li><a class= "sign__link" href="#">${headerList[2]}</a></li>
</ul>
`;
  container.classList.toggle("landing");
  container.classList.toggle("signUp");
  main.innerHTML = `
  <section class="section-signUp">
  <form>  
    <fieldset>  
       <label>Name:</label>
       <input type="text" name="name">
        <label>Password:</label> 
        <input type="current-password" name="password">
        <label>e-mail:</label>
        <input type="email" name="email"> 
      </fieldset>  
 </form>   
  <button class="btn">Join now</button>
</section>
  `;
  document
    .querySelector(".discover__link-signUp")
    .addEventListener("click", renderLandingPage);
}

function renderFeaturePage() {
  container.classList.remove("landing");
  container.classList.remove("signUp");
  headerNav.innerHTML = `
<ul class="header__nav-menu">
  <li><a class= "join__link" href="#">${headerList[1]}</a></li>
  <li><a class= "sign__link" href="#">${headerList[2]}</a></li>
</ul>
`;
  main.innerHTML = `
  <section class="section-feature">
  <div class="section-feature-left">
    <h1 class="title">Discover new music</h1>

    <div class="section-feature-btns">
      <button class="btn">Charts</button>
      <button class="btn">Songs</button>
      <button class="btn">Artists</button>
    </div>

    <div class="feature__text">
      By joining you can benefit by listening to the latest albums released
    </div>
  </div>

  <div class="feature__img">
    <img
      src="./assets/images/music-titles.png"
      alt="feature image"
      height="512"
      width="512"
    />
  </div>
</section>
`;
  document
    .querySelector(".join__link")
    .addEventListener("click", renderSignUpPage);
}

